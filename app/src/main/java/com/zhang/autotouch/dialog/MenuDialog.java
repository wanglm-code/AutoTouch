package com.zhang.autotouch.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zhang.autotouch.R;
import com.zhang.autotouch.TouchEventManager;
import com.zhang.autotouch.adapter.AutoItemAdapter;
import com.zhang.autotouch.adapter.TouchPointAdapter;
import com.zhang.autotouch.bean.AppInfo;
import com.zhang.autotouch.bean.AutoEvent;
import com.zhang.autotouch.bean.AutoItem;
import com.zhang.autotouch.bean.AutoItemTypeEnum;
import com.zhang.autotouch.bean.ScreenInfo;
import com.zhang.autotouch.bean.TouchEvent;
import com.zhang.autotouch.bean.TouchPoint;
import com.zhang.autotouch.utils.DensityUtil;
import com.zhang.autotouch.utils.DialogUtils;
import com.zhang.autotouch.utils.GsonUtils;
import com.zhang.autotouch.utils.SpUtils;
import com.zhang.autotouch.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class MenuDialog extends BaseServiceDialog implements View.OnClickListener {

    // wanglm
    private TextView tvLog;         // 日志
    private Button btStart;         // 开始脚本
    private Button btEnd;           // 停止脚本
    private RecyclerView rvList;    // 脚本内容项
    private List<AutoItem> autoItems;   // 具体的脚本
    private View overlay;           // 叠加层
    private AutoItemAdapter autoItemAdapter;
    // end wanglm

    private Button btStop;
    private RecyclerView rvPoints;

    private AddPointDialog addPointDialog;
    private Listener listener;
    private TouchPointAdapter touchPointAdapter;
    private RecordDialog recordDialog;

    public MenuDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_menu;
    }

    @Override
    protected int getWidth() {
        return DensityUtil.dip2px(getContext(), 350);
    }

    @Override
    protected int getHeight() {
        return WindowManager.LayoutParams.WRAP_CONTENT;
    }

    /**
     * 展示日志
     * @param content
     */
    private void showLog(String content){
        String c = tvLog.getText().toString();
        if(c.length() > 6000)
            tvLog.setText(content);
        else
            tvLog.setText(c+"\r\n"+content);
    }

    @Override
    protected void onInited() {
        setCanceledOnTouchOutside(true);

        // wanglm
        tvLog = findViewById(R.id.tv_log);
        overlay = findViewById(R.id.overlay);
        overlay.setOnClickListener(v->{}); // 取消所有的点击事件
        overlay.setVisibility(View.GONE);
        showLog(ScreenInfo.getInstance().toString());
        btStart = findViewById(R.id.bt_start);
        btStart.setOnClickListener(this);
        btEnd = findViewById(R.id.bt_end);
        btEnd.setOnClickListener(this);
        if(autoItems == null) {
            autoItems = new ArrayList<>();
            autoItems.add(new AutoItem("师门", AutoItemTypeEnum.SHIMEN));
            autoItems.add(new AutoItem("捉鬼", AutoItemTypeEnum.ZHUOGUI));
            autoItems.add(new AutoItem("宝图", AutoItemTypeEnum.BAOTU){
                @Override
                public boolean isEnable() {
                    return true;
                }
            });
        }
        rvList = findViewById(R.id.rv_list);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        autoItemAdapter = new AutoItemAdapter(autoItems);
        rvList.setAdapter(autoItemAdapter);
        // end wanglm

        findViewById(R.id.bt_exit).setOnClickListener(this);
        findViewById(R.id.bt_add).setOnClickListener(this);
        findViewById(R.id.bt_record).setOnClickListener(this);
        btStop = findViewById(R.id.bt_stop);
        btStop.setOnClickListener(this);
        rvPoints = findViewById(R.id.rv);
        touchPointAdapter = new TouchPointAdapter();
        touchPointAdapter.setOnItemClickListener(new TouchPointAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, TouchPoint touchPoint) {
                btStop.setVisibility(View.VISIBLE);
                dismiss();
                TouchEvent.postStartAction(touchPoint);
                ToastUtil.show("已开启触控点：" + touchPoint.getName());
            }
        });
        rvPoints.setLayoutManager(new LinearLayoutManager(getContext()));
        rvPoints.setAdapter(touchPointAdapter);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (TouchEventManager.getInstance().isPaused()) {
                    TouchEvent.postContinueAction();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("啊实打实", "onStart");
        //如果正在触控，则暂停
        TouchEvent.postPauseAction();
        if (touchPointAdapter != null) {
            List<TouchPoint> touchPoints = SpUtils.getTouchPoints(getContext());
            Log.d("啊实打实", GsonUtils.beanToJson(touchPoints));
            touchPointAdapter.setTouchPointList(touchPoints);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_start:
                btEnd.setVisibility(View.VISIBLE);
                btStart.setVisibility(View.GONE);
                overlay.setVisibility(View.VISIBLE);
                dismiss();
                AutoEvent.postStartAction(autoItems);
                ToastUtil.show("已开启后台脚本");
                AppInfo.getInstance().setRunState(true);
                break;
            case R.id.bt_end:
                btEnd.setVisibility(View.GONE);
                btStart.setVisibility(View.VISIBLE);
                overlay.setVisibility(View.GONE);
                AutoEvent.postEndAction();
                ToastUtil.show("已停止后台脚本");
                AppInfo.getInstance().setRunState(false);
                break;
            case R.id.bt_add:
                DialogUtils.dismiss(addPointDialog);
                addPointDialog = new AddPointDialog(getContext());
                addPointDialog.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        MenuDialog.this.show();
                    }
                });
                addPointDialog.show();
                dismiss();
                break;
            case R.id.bt_record:
                dismiss();
                if (listener != null) {
                    listener.onFloatWindowAttachChange(false);
                    if (recordDialog ==null) {
                        recordDialog = new RecordDialog(getContext());
                        recordDialog.setOnDismissListener(new OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                listener.onFloatWindowAttachChange(true);
                                MenuDialog.this.show();
                            }
                        });
                        recordDialog.show();
                    }
                }
                break;
            case R.id.bt_stop:
                btStop.setVisibility(View.GONE);
                TouchEvent.postStopAction();
                ToastUtil.show("已停止触控");
                break;
            case R.id.bt_exit:
                TouchEvent.postStopAction();
                if (listener != null) {
                    listener.onExitService();
                }
                break;

        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        /**
         * 悬浮窗显示状态变化
         * @param attach
         */
        void onFloatWindowAttachChange(boolean attach);

        /**
         * 关闭辅助
         */
        void onExitService();
    }
}
