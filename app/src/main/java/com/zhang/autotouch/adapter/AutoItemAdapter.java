package com.zhang.autotouch.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zhang.autotouch.R;
import com.zhang.autotouch.bean.AutoItem;

import java.util.List;

public class AutoItemAdapter extends RecyclerView.Adapter<AutoItemAdapter.ViewHolder> {

    private List<AutoItem> autoItems;

    public AutoItemAdapter(List<AutoItem> autoItems){
        this.autoItems = autoItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_auto, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AutoItem ai = autoItems.get(position);
        holder.cbEnable.setChecked(ai.isEnable());
        holder.tvState.setText(ai.getState());
        holder.tvName.setText(ai.getName());
    }

    @Override
    public int getItemCount() {
        return autoItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvName;
        TextView tvState;
        CheckBox cbEnable;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvState = itemView.findViewById(R.id.tv_state);
            cbEnable = itemView.findViewById(R.id.cb_enable);
        }
    }
}
