package com.zhang.autotouch.bean;

import androidx.annotation.NonNull;

import com.zhang.autotouch.utils.GsonUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class AutoEvent {

    public static final int ACTION_START = 1;
    public static final int ACTION_PAUSE = 2;
    public static final int ACTION_CONTINUE = 3;
    public static final int ACTION_STOP = 4;
    public static final int ACTION_END = 5;

    private int action;
    private List<AutoItem> autoItems;

    private AutoEvent(int action) {
        this(action, null);
    }

    private AutoEvent(int action, List<AutoItem> autoItems) {
        this.action = action;
        this.autoItems = autoItems;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getAction() {
        return action;
    }

    public List<AutoItem> getAutoItems() {
        return autoItems;
    }

    public static void postStartAction(List<AutoItem> autoItems) {
        postAction(new AutoEvent(ACTION_START, autoItems));
    }

    public static void postPauseAction() {
        postAction(new AutoEvent(ACTION_PAUSE));
    }

    public static void postEndAction(){
        postAction(new AutoEvent(ACTION_END));
    }

    public static void postContinueAction() {
        postAction(new AutoEvent(ACTION_CONTINUE));
    }

    public static void postStopAction() {
        postAction(new AutoEvent(ACTION_STOP));
    }

    private static void postAction(AutoEvent autoEvent) {
        EventBus.getDefault().post(autoEvent);
    }

    @NonNull
    @Override
    public String toString() {
        return "action=" + action + " autoItems=" + (autoItems == null ? "null" : GsonUtils.beanToJson(autoItems));
    }
}
