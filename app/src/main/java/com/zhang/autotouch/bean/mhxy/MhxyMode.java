package com.zhang.autotouch.bean.mhxy;

public enum MhxyMode {
    /**
     * 正常模式
     */
    NORMAL,
    /**
     * 战斗模式
     */
    BATTLE,
    /**
     * 其他模式，此处一般指弹窗一类的内容
     */
    OTHER,
}
