package com.zhang.autotouch.bean.mhxy;

public enum MhxyLoopState {
    /**
     * 初始状态
     */
    None,
    /**
     * 持续运行
     */
    Going,
    /**
     * 错误
     */
    Error,
    /**
     * 失败
     */
    Fail,
    /**
     * 运行结束
     */
    Finish
}
