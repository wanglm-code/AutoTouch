package com.zhang.autotouch.bean;

import lombok.Data;

/**
 * 自动测试项
 */
@Data
public class AutoItem {
    private String name;
    private AutoItemTypeEnum type;
    private boolean enable;
    private String state;
    private int delay;

    public AutoItem(String name, AutoItemTypeEnum type){
        this.name = name;
        this.type = type;
        this.enable = false;
        this.state = "待执行";
        this.delay = 5;
    }

    public boolean action(){
        switch (this.type){
            case SHIMEN:
                break;
            case ZHUOGUI:
                break;
        }

        return true;
    }
}
