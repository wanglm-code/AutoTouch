package com.zhang.autotouch.bean.mhxy;

import android.graphics.Rect;

import com.zhang.autotouch.bean.ScreenInfo;

/**
 * 梦幻西游手游屏幕分布信息
 */
public class MhxyScreenInfo {

    private static class MhxyScreenInfoHolder{
        public static MhxyScreenInfo instance = new MhxyScreenInfo();
    }

    private MhxyScreenInfo(){

    }

    public static MhxyScreenInfo getInstance(){
        return MhxyScreenInfo.MhxyScreenInfoHolder.instance;
    }

    private Rect huodong;
    /**
     * 获取活动/挂机界面的位置，当前为行/3,列/2
     * 可以加速6倍识别
     * @return
     */
    public Rect getHuodong(){
        if(huodong == null){
            huodong = new Rect();
            huodong.left = 0;
            huodong.top = 0;
            huodong.right = ScreenInfo.getInstance().getScreenWidth()/2;
            huodong.bottom = ScreenInfo.getInstance().getScreenHeight()/3;
        }

        return huodong;
    }

    private Rect tasks;

    /**
     * 获取任务的界面位置，当前为右边三分之一
     * @return
     */
    public Rect getTasks(){
        if(tasks != null){
            tasks = new Rect();
            tasks.left = ScreenInfo.getInstance().getScreenWidth()/3;
            tasks.top = 0;
            tasks.right = ScreenInfo.getInstance().getScreenWidth();
            tasks.bottom = ScreenInfo.getInstance().getScreenHeight();
        }

        return tasks;
    }
}
