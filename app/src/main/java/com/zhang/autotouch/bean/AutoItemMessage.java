package com.zhang.autotouch.bean;

import android.util.Log;

import com.benjaminwan.ocrlibrary.TextBlock;

import io.reactivex.rxjava3.core.ObservableEmitter;
import lombok.Data;

@Data
public class AutoItemMessage {

    public final static String TAG = "任务消息";

    public enum MessageType{
        SHOW, CLICK, SWIPE
    }

    public AutoItemMessage(ObservableEmitter<AutoItemMessage> emitter){
        this.emitter = emitter;
    }

    private ObservableEmitter<AutoItemMessage> emitter;
    private MessageType type;
    private String content;
    private float x;
    private float y;
    private float x1;
    private float y1;
    private int duration = 200;
    private int startTime = 0;
    private AutoItem autoItem;

    private void showProcess(AutoItem autoItem){
        emitter.onNext(this);
        Log.i(TAG, autoItem.getName()+" - "+ this.toString());
    }

    private void showProcess(){
        emitter.onNext(this);
        Log.i(TAG, autoItem.getName()+" - "+ this.toString());
    }

    /**
     * 展示具体的消息
     * @param content
     */
    public void showMessage(String content, int delay){
        this.content = autoItem.getName()+" - "+ content;
        this.type = MessageType.SHOW;
        Log.i(TAG, this.content);
        emitter.onNext(this);

        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 展示具体的消息，默认延迟1秒
     * @param content
     */
    public void showMessage(String content){
        showMessage(content, 1000);
    }

    /**
     * 滑动消息
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param content
     * @param delay
     */
    public void SwipeMessage(float x1, float y1, float x2, float y2, String content, int delay){
        this.x = x1;
        this.y = y1;
        this.x1 = x2;
        this.y1 = y2;
        this.duration = 500;
        this.startTime = 0;
        this.content = autoItem.getName()+" - "+ content + " [" + x + "," + y + "]";
        this.type = MessageType.SWIPE;
        Log.i(TAG, this.content);
        emitter.onNext(this);
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 点击消息
     * @param content
     */
    public void clickMessage(float x, float y, String content, int delay){
        this.x = x;
        this.y = y;
        this.duration = 200;
        this.startTime = 0;
        this.content = autoItem.getName()+" - "+ content + " [" + x + "," + y + "]";
        this.type = MessageType.CLICK;
        Log.i(TAG, this.content);
        emitter.onNext(this);

        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 点击消息
     * @param content
     */
    public void clickMessage(TextBlock tb, String content){
        clickMessage(new SearchPoint(tb.getBoxPoint()), content);
    }

    /**
     * 点击消息
     * @param content
     */
    public void clickMessage(SearchPoint sp, String content){
        clickMessage(sp.getClickX(), sp.getClickY(), content, 1000);
    }

    /**
     * 点击消息
     * @param content
     */
    public void clickMessage(float x, float y, String content){
        clickMessage(x, y, content, 1000);
    }

    /**
     * 滑动消息
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param content
     */
    public void SwipeMessage(float x1, float y1, float x2, float y2, String content){
        SwipeMessage(x1, y1, x2, y2, content, 1000);
    }

    @Override
    public String toString(){
        return content;
    }
}
