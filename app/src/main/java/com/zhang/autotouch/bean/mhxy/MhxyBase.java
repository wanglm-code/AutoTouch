package com.zhang.autotouch.bean.mhxy;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import com.benjaminwan.ocrlibrary.OcrResult;
import com.benjaminwan.ocrlibrary.TextBlock;
import com.zhang.autotouch.App;
import com.zhang.autotouch.bean.AppInfo;
import com.zhang.autotouch.bean.AutoItem;
import com.zhang.autotouch.bean.AutoItemMessage;
import com.zhang.autotouch.bean.GameManage;
import com.zhang.autotouch.bean.ScreenInfo;
import com.zhang.autotouch.bean.SearchPoint;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

/**
 * 所有任务的基类
 */
public class MhxyBase {

    /**
     * 每个任务的超时时间
     */
    public static long ItemTimeout = 10*60*1000;

    private static final String TAG = App.class.getSimpleName();

    protected GameManage gameManage;
    protected AutoItemMessage autoItemMessage;

    protected Bitmap currentBitmap;
    protected Mat currentBitmapMat = new Mat();
    protected Bitmap taskBitmap;
    protected Bitmap huodongBitmap;
    protected MhxyMode mode;

    protected OcrResult taskBitmapOcr;
    protected OcrResult huodongBitmapOcr;
    /**
     * 队伍的坐标，必须处于 Normal 模式下
     */
    protected SearchPoint spDuiwu;
    /**
     * 任务的坐标，必须处于 Normal 模式下
     */
    protected SearchPoint spRenwu;

    /**
     * 初始化
     * @param gameManage
     * @param autoItemMessage
     */
    public MhxyBase(GameManage gameManage, AutoItemMessage autoItemMessage){
        this.gameManage = gameManage;
        this.autoItemMessage = autoItemMessage;
    }

    /**
     * 在task里面查找指定的文本
     * @param text 查找的文本
     * @return
     */
    public TextBlock getTaskOcr(String text){
        if(taskBitmapOcr.getTextBlocks().size()>0){
            for (TextBlock tb :
                    taskBitmapOcr.getTextBlocks()) {
                if(tb.getText().contains(text)){
                    return tb;
                }
            }
        }
        return null;
    }

    /**
     * 在 huodong 里面查找指定的文本
     * @param text 查找文本
     * @return
     */
    public TextBlock getHuodongOcr(String text){
        if(huodongBitmapOcr.getTextBlocks().size()>0){
            for (TextBlock tb :
                    huodongBitmapOcr.getTextBlocks()) {
                if(tb.getText().contains(text)){
                    return tb;
                }
            }
        }
        return null;
    }

    /**
     * 是否领取宝图任务，主要检查task里面是否存在《宝图任务》四个字
     * @return
     */
    public TextBlock getHuodongOcr(){
        TextBlock tb = this.getHuodongOcr("活动");
        if(tb == null){
            return null;
        }
        return tb;
    }

    /**
     * 如果在右侧存在任务和队伍，则可以任务在正常模式
     * @param bitmap
     * @return
     */
    public boolean isNormal(Bitmap bitmap){
        boolean isRenwu = false;
        boolean isDuiwu = false;
        if(taskBitmapOcr.getTextBlocks().size()>0){
            for (TextBlock tb :
                    taskBitmapOcr.getTextBlocks()) {
                if(tb.getText().contains("任务")){
                    isRenwu = true;
                    spRenwu = new SearchPoint(tb.getBoxPoint());
                } else if(tb.getText().contains("队伍")){
                    isDuiwu = true;
                    spDuiwu = new SearchPoint(tb.getBoxPoint());
                }
            }
        }
        if(isDuiwu && isRenwu)
            return true;
        return false;
    }

    /**
     * 关闭弹窗
     */
    public void closeWindow(){
        for (int i = 0; i < 10; i++) {
            updateCurrentBitmap();
            SearchPoint sp = gameManage.SearchCloseWindow(currentBitmapMat);
            if(sp == null){
                Log.i(TAG, "closeWindow: 未找到关闭按钮，无需关闭 - "+(i+1));
                return;
            } else{
                Log.i(TAG, "closeWindow: 找到关闭按钮 - "+(i+1));
                autoItemMessage.clickMessage(sp, "关闭弹窗 - "+(i+1));
            }
        }

        Log.e(TAG, "closeWindow: 关闭按钮失败 - 超过10次");
    }

    /**
     * 更新当前的截图
     */
    public void updateCurrentBitmap(){
        // 获取当前截图
        if(currentBitmap != null){
            currentBitmap.recycle();
        }
        currentBitmap = gameManage.getBitmap();
        Utils.bitmapToMat(currentBitmap, currentBitmapMat);
    }

    /**
     * 更新 Task OCR内容
     */
    public void updateTaskOcr(){
        Rect r = MhxyScreenInfo.getInstance().getTasks();
        if(taskBitmap != null){
            taskBitmap.recycle();
        }
        taskBitmap = Bitmap.createBitmap(currentBitmap, r.left, r.top, r.width(), r.height(), null, false);
        taskBitmapOcr = App.getInstance().detect(taskBitmap);
    }

    /**
     * 更新 活动 OCR内容
     */
    public void updateHudongOcr(){
        Rect r = MhxyScreenInfo.getInstance().getHuodong();
        if(huodongBitmap != null){
            huodongBitmap.recycle();
        }
        huodongBitmap = Bitmap.createBitmap(currentBitmap, r.left, r.top, r.width(), r.height(), null, false);
        huodongBitmapOcr = App.getInstance().detect(huodongBitmap);
    }

    /**
     * 打开活动页面
     * @return
     */
    public boolean openHudongPage(){
        updateHudongOcr();
        TextBlock tbHuodong = this.getHuodongOcr();
        if(tbHuodong == null){
            // 找不动活动按钮
            Log.e(TAG, "领取任务失败，打不开活动界面");
            return false;
        }
        autoItemMessage.clickMessage(new SearchPoint(tbHuodong.getBoxPoint()), "点击活动按钮");
        return true;
    }

    /**
     * 滑动活动节目，从中间开始，上画1/3节目
     */
    public void swipeHuodongPage(){
        float height = ScreenInfo.getInstance().getScreenHeight();
        float width = ScreenInfo.getInstance().getScreenWidth();
        float x1 = width/2 + (float) Math.random()*10-5;
        float y1 = height/2 + (float) Math.random()*10-5;

        float x2 = x1 +(float) Math.random()*10-5;
        float y2 = y1 + height/3+ height/2 + (float) Math.random()*10-5;

        autoItemMessage.SwipeMessage(x1, y1, x2, y2, "上滑活动界面");
    }

    /**
     * 点击参加，因为截图右边是参加按钮，所以直接点击右边即可
     * @param sp
     */
    public void clickHuodongCanjia(SearchPoint sp){
        float x1 = (float) sp.getPointC()[0]-(float) Math.random()*10;
        float y1 = sp.getClickY();
        autoItemMessage.clickMessage(x1, y1, "点击参加");
    }

    public MhxyLoopState loop(){
        return MhxyLoopState.None;
    }
}
