package com.zhang.autotouch.bean;

import com.zhang.autotouch.service.ScreenRecordService;

import lombok.Data;

@Data
public class AppInfo {

    public static class AppInfoHolder{
        public static AppInfo instance = new AppInfo();
    }

    private AppInfo(){}

    public static AppInfo getInstance(){
        return AppInfoHolder.instance;
    }

    // 截图运行状态
    private boolean capRunState;

    // 脚本运行状态
    private boolean runState;

    // 自动实时状态
    private int autoAction;
}
