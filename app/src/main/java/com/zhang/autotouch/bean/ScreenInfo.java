package com.zhang.autotouch.bean;

import lombok.Data;

@Data
public class ScreenInfo {

    private static class ScreenInfoHolder{
        public static ScreenInfo instance = new ScreenInfo();
    }

    private ScreenInfo(){
        this.isAudio = false;
        this.isVideoSd = true;
    }

    public static ScreenInfo getInstance(){
        return ScreenInfoHolder.instance;
    }

    // 屏幕相关信息
    private int screenWidth;
    private int screenHeight;
    private int screenDensity;
    /** 是否为标清视频 */
    private boolean isVideoSd;
    /** 是否开启音频录制 */
    private boolean isAudio;

    public void init(int screenWidth, int screenHeight, int screenDensity){
        this.screenDensity = screenDensity;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
    }

    @Override
    public String toString() {
        return "屏幕信息:" +
                "Width=" + screenWidth +
                ", Height=" + screenHeight +
                ", Density=" + screenDensity;
    }
}
