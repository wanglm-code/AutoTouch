package com.zhang.autotouch.bean.mhxy;

import android.util.Log;

import com.benjaminwan.ocrlibrary.TextBlock;
import com.zhang.autotouch.bean.AutoItemMessage;
import com.zhang.autotouch.bean.GameManage;
import com.zhang.autotouch.bean.SearchPoint;

public class MhxyBaotu extends MhxyBase {
    private static final String TAG = "宝图任务";

    /**
     * 初始化
     *
     * @param gameManage
     * @param autoItemMessage
     */
    public MhxyBaotu(GameManage gameManage, AutoItemMessage autoItemMessage) {
        super(gameManage, autoItemMessage);
    }

    /**
     * 是否领取宝图任务，主要检查task里面是否存在《宝图任务》四个字
     * @return
     */
    public TextBlock isLingqu(){
        TextBlock tb = this.getTaskOcr("宝图任务");
        if(tb == null){
            return null;
        }
        return tb;
    }

    @Override
    public MhxyLoopState loop() {
        // 关闭意外窗口
        closeWindow();
        // 是否领取宝图任务
        TextBlock tb = isLingqu();
        if(tb == null){
            // 说明没有领取，此时需要去领取任务
            // 更新活动坐标
            if(!openHudongPage()){
                return MhxyLoopState.Error;
            }
            updateCurrentBitmap();
            SearchPoint spBaotu = gameManage.searchLqBaotu(this.currentBitmapMat);
            if(spBaotu == null){
                // 不存在宝图任务，上划界面
                Log.i(TAG, "未发现宝图任务，上滑界面查找 1");
                swipeHuodongPage();
                spBaotu = gameManage.searchLqBaotu(this.currentBitmapMat);
                if(spBaotu == null){
                    // 不存在宝图任务，上划界面
                    Log.i(TAG, "未发现宝图任务，上滑界面查找 2");
                    swipeHuodongPage();
                    spBaotu = gameManage.searchLqBaotu(this.currentBitmapMat);
                    if(spBaotu == null){
                        Log.e(TAG, "未发现宝图任务");
                        return MhxyLoopState.Error;
                    }
                }
            }
            clickHuodongCanjia(spBaotu);
            for (int i = 0; i < 11; i++) {
                Log.i(TAG, "查找听听无妨");
                updateCurrentBitmap();
                updateTaskOcr();
                TextBlock t = this.getTaskOcr("听听无妨");
                if(t != null){
                    Log.i(TAG, "点击听听无妨");
                    autoItemMessage.clickMessage(new SearchPoint(t.getBoxPoint()), "点击听听无妨");
                    updateCurrentBitmap();
                    updateTaskOcr();
                }
                if(i == 10){
                    Log.e(TAG, "未发现听听无妨");
                    return MhxyLoopState.Error;
                }
            }
        }

        // 点击宝图任务
        Log.i(TAG, "开始执行宝图任务");
        autoItemMessage.clickMessage(tb, "开始执行宝图任务");
        // 检测是否结束
        long start = System.currentTimeMillis();
        long end = start;
        while (end - start < MhxyBase.ItemTimeout){
            updateCurrentBitmap();
            updateTaskOcr();
            tb = isLingqu();
            end = System.currentTimeMillis();
            if(tb == null){
                Log.i(TAG, "结束执行宝图任务");
                autoItemMessage.showMessage("宝图任务结束 "+(end-start)/1000);
                return MhxyLoopState.Finish;
            }
            autoItemMessage.showMessage("宝图任务，执行时间 "+(end-start)/1000);
        }

        autoItemMessage.showMessage("宝图任务，超时 "+(end-start)/1000);

        return MhxyLoopState.Fail;
    }
}
